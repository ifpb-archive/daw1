// função para criar um novo elemento no documento html
function novoElemento(tagName,className){ // recebe como argumento o nome da tag (exemplo: div) e da classe
    const elem = document.createElement(tagName);
    elem.className = className;
    return elem; // retorna o elemento criado
}

// função para criar uma nova barreira composta pelos elementos borda e corpo
function Barreira(reversa){
    this.elemento = novoElemento('div','barreira');

    const borda = novoElemento('div','borda');
    const corpo = novoElemento('div','corpo');

    this.elemento.appendChild(reversa? corpo : borda); /* se a barreira for reversa, insere o elemento corpo nela 
                                                          se a barreira não for reversa, insere o elemento borda nela */
    this.elemento.appendChild(reversa? borda : corpo); /* se a barreira for reversa, insere o elemento borda nela,
                                                          se a barreira não for reversa, insere o elemento corpo nela */

    this.setAltura = altura => corpo.style.height = `${altura}px`; // seta a altura da barreira de acordo a definida no arquivo flappy.css
}

//  const b = new Barreira(true);
//  b.setAltura(220);
//  document.querySelector('[wm-flappy]').appendChild(b.elemento);

// função para criar a div par de barreiras, que tem como elementos filho as duas barreiras
function ParDeBarreiras(altura, abertura, x){
    this.elemento = novoElemento('div','par-de-barreiras');

    this.superior = new Barreira(true); // barreira superior e reversa
    this.inferior = new Barreira(false); // barreira inferior e não reversa

    this.elemento.appendChild(this.superior.elemento);
    this.elemento.appendChild(this.inferior.elemento);

    // função que define valores aleatórios para a altura das barreiras, animando-as no jogo
    this.sortearAbertura = () =>{
        const alturaSuperior = Math.random() * (altura - abertura);
        const alturaInferior = altura - abertura - alturaSuperior;

        this.superior.setAltura(alturaSuperior);
        this.inferior.setAltura(alturaInferior);
    }

    // funções que buscam o posicionamento e largura dos canos, para identificar quando houver colisão com o pássaro no jogo
    this.getX = () => parseInt(this.elemento.style.left.split('px')[0]);
    this.setX = x => this.elemento.style.left = `${x}px`;
    this.getLargura = () => this.elemento.clientWidth;

    this.sortearAbertura();
    this.setX(x);

}

// const b = new ParDeBarreiras(700, 200, 400); // cria novo par de barreiras com altura de 700 pixels, abertura de 200 pixels e posição de 400 pixels
// document.querySelector('[wm-flappy]').appendChild(b.elemento); // adiciona o par de barreiras criado no arquivo html como elemento filho da div wm-flappy

// função para criar quatro barreiras que recebe cinco parâmetros, entre eles o espaço entre essas barreiras
function Barreiras(altura, largura, abertura, espaco, notificarponto){
    // em vez de criar vários pares de barreiras com posições diferentes, definimos um array que vai ficar se repetindo
    this.pares = [
        new ParDeBarreiras(altura, abertura, largura),
        new ParDeBarreiras(altura, abertura, largura + espaco),
        new ParDeBarreiras(altura, abertura, largura + espaco*2),
        new ParDeBarreiras(altura, abertura, largura + espaco*3)
    ];

    const deslocamento = 3; // as barreiras se deslocam com uma velocidade de 3 pixels (quanto maior, mais rápido)

    // método para fazer com que cada barreira mude de posição no decorrer do tempo, de acordo com a velocidade do deslocamento
    this.animar = () => {
        this.pares.forEach(par => {
            par.setX(par.getX() - deslocamento); // erro no getX (o x estava minúsculo)

            // se o par de barreiras já tiver passado pela tela, ele vai ser repetido (dessa vez, com uma abertura diferente)
            if(par.getX() < - par.getLargura()){
                par.setX(par.getX() + espaco * this.pares.length);
                par.sortearAbertura();
            }
            const meio = largura/2;
            // se o passarinho passar pelo par de barreiras sem colidir nele, cruzouOMeio retorna true e o aumento da pontuação será notificado
            const cruzouOMeio = par.getX() + deslocamento >= meio && par.getX() < meio;
            if (cruzouOMeio) notificarponto();
        });
   } 

}

// cria várias barreiras e adiciona cada par na div [wm-flappy] como elemento filho do elemento arenaDoJogo
const barreiras = new Barreiras(700, 1200, 200, 400);
const arenaDoJogo = document.querySelector('[wm-flappy]');
barreiras.pares.forEach(par => arenaDoJogo.appendChild(par.elemento));

setInterval(() => {
    barreiras.animar() // chama o método animar() a cada 20 milésimos de segundo
},20);